Source: xfonts-jmk
Section: fonts
Priority: optional
Maintainer: Russ Allbery <rra@debian.org>
Standards-Version: 4.5.1
Build-Depends:
 debhelper-compat (= 13),
 xfonts-utils,
 xutils-dev,
Rules-Requires-Root: no
Homepage: http://nikolas.us.to/jmkfonts/
Vcs-Git: https://salsa.debian.org/rra/xfonts-jmk.git
Vcs-Browser: https://salsa.debian.org/rra/xfonts-jmk

Package: xfonts-jmk
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Jim Knoble's character-cell fonts for X
 These are character-cell fonts for use with the X Window System, created
 by Jim Knoble.  The fonts currently included in this package are:
 .
 Neep (formerly known as NouveauGothic): A pleasantly legible variation on
 the standard fixed fonts that accompany most distributions of the X
 Window System.  Comes in both normal and bold weights in small, medium,
 large, extra-large, and huge sizes, as well as an extra-small size that
 only comes in normal weight.  ISO-8859-1, ISO-8859-2, ISO-8859-9, and
 ISO-8859-15 encodings are available.  The 6x13, 8x15, and 10x20 sizes have
 an ISO-10646-1 (Unicode) variant, drawing glyphs missing in Neep from
 misc-fixed.
 .
 Modd: A fixed-width font with sleek, contemporary styling.  Normal and
 bold weights in a 12-point (6x13) size.  ISO-8859-1 encoding only.
